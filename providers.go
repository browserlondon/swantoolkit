package toolkit

import (
	"encoding/json"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	log "github.com/sirupsen/logrus"
)

type (
	DataProviderMap map[string]DataProvider

	DataProvider struct {
		Id             string                        `json:"id"`
		Label          string                        `json:"label"`
		Enabled        bool                          `json:"enabled"`
		Type           string                        `json:"type"`
		Facets         []DataProviderFacet           `json:"facets"`
		Dispatch       DataProviderDispatch          `json:"dispatch"`
		Mappings       DataProviderMapping           `json:"mappings"`
		MetadataFacets []MetaFacetMapping            `json:"metadata_facets"`
		References     []DataProviderReferenceFormat `json:"references"`
		Layouts        map[string]string             `json:"layouts"`
	}

	DataProviderFacet struct {
		Id      string `json:"id"`
		Label   string `json:"label"`
		Type    string `json:"type"`
		Mapping string `json:"mapping"`
	}

	DataProviderDispatch struct {
		Query            string   `json:"query"`
		Fetch            string   `json:"fetch"`
		RequiresFetch    bool     `json:"requires_fetch"`
		FetchSize        int      `json:"fetch_size"`
		Retrieve         string   `json:"retrieve"`
		RequiresRetrieve bool     `json:"requires_retrieve"`
		RetrieveSize     int      `json:"retrieve_size"`
		RetrieveEnabled  bool     `json:"retrieve_enabled"`
		Related          string   `json:"related"`
		RelatedEnabled   bool     `json:"related_enabled"`
		Translate        string   `json:"translate"`
		MoreFirst        int      `json:"more_first"`       // how many pages to get when first query returned
		MorePerPage      int      `json:"more_per_page"`    // how many extra pages to get per page request
		SortDefault      string   `json:"sort_default"`     // if empty defaults to "when-desc"
		SortOptions      []string `json:"sort_options"`     // if set filters sort-by (else assumes default options)
		CollapseResults  string   `json:"collapse_results"` // option to override default collapse handling
		AdverseTerms     bool     `json:"adverse_terms"`    // if adverse terms is enabled
		LocalFilter      bool     `json:"local_filter"`     // if local filter is enabled (else calls provider API each time)
	}

	DataProviderMapping struct {
		IsStructuredJson bool   `json:"is_structured_json"`
		Iterator         string `json:"iterator"`
		FetchIterator    string `json:"fetch_iterator"`
		FetchId          string `json:"fetch_id"`
		Id               string `json:"id"`
		Headline         string `json:"headline"`
		HeadlineTags     string `json:"headline_tags"`
		Date             string `json:"date"`
		DateFormat       string `json:"date_format"`
		Source           string `json:"source"`
		Preview          string `json:"preview"`
		PreviewTags      string `json:"preview_tags"`
		Url              string `json:"url"`
		EndUrl           string `json:"end_url"`
		Content          string `json:"content"`
		Language         string `json:"language"`
		DuplicateHash    string `json:"duplicate_hash"`
		RelatedUrl       string `json:"related_url"`
		Helpers          string `json:"helpers,omitempty"`
	}

	MetaFacetMapping struct {
		GroupIterator       string                 `json:"group_iterator"`
		BucketIterator      string                 `json:"bucket_iterator"`
		ProviderControl     string                 `json:"provider_control"`
		ControlType         string                 `json:"control_type"`
		Label               string                 `json:"label"`                  // Can be a simple string or a mapping
		LabelIsGroupMapping bool                   `json:"label_is_group_mapping"` // If the Label field a JSON mapping or a string?
		BucketMapping       MetaFacetBucketMapping `json:"bucket_mapping"`
	}

	MetaFacetBucketMapping struct {
		Value   string `json:"value"`
		Display string `json:"display"`
		Count   string `json:"count"`
	}

	DataProviderReferenceFormat struct {
		Label        string `json:"label"`
		DocumentType string `json:"document_type"`
		Format       string `json:"format"`
		DateFormat   string `json:"date_format"`
	}
)

func LoadProvidersFromS3(region string, bucket string, prefix string) (providers DataProviderMap) {

	providers = make(DataProviderMap, 0)

	sess, _ := session.NewSession(&aws.Config{
		Region: aws.String(region),
	})

	svc := s3.New(sess)
	input := &s3.ListObjectsInput{
		Bucket:  aws.String(bucket),
		MaxKeys: aws.Int64(100),
		Prefix:  aws.String(prefix),
	}

	result, err := svc.ListObjects(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case s3.ErrCodeNoSuchBucket:
				log.WithError(aerr).Fatal(s3.ErrCodeNoSuchBucket)
			default:
				log.WithError(aerr).Fatal("Could not get S3 objects")
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			log.WithError(aerr).Fatal("Could not get S3 objects")
		}
		return
	}

	if len(result.Contents) > 0 {

		downloader := s3manager.NewDownloader(sess)

		// Fetch each provider config, unmarshal and hold in memory
		for _, obj := range result.Contents {

			buf := aws.NewWriteAtBuffer([]byte{})
			downloader.Download(buf, &s3.GetObjectInput{
				Bucket: result.Name,
				Key:    obj.Key,
			})

			provider := DataProvider{}
			err = json.Unmarshal(buf.Bytes(), &provider)
			if err != nil {
				log.WithField("key", obj.Key).WithError(err).Error("Could not unmarshal data provider config")
			} else {
				providers[provider.Id] = provider
				log.WithField("Provider", provider.Id).Info("Loading provider config")
			}
		}
	} else {
		log.Warning("No provider configs loaded")
	}

	return providers
}
