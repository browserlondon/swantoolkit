package toolkit

import (
	"fmt"
	"time"
)

// Create simple IDs to use in logs to help track multi-threaded events
func MakeJobId() string {
	now := time.Now()
	sec := now.Unix()
	usec := now.UnixNano() % 0x100000

	return fmt.Sprintf("%08x%05x", sec, usec)
}
