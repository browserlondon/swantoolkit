package advancedsearch

import (
	"errors"
	"time"

	toolkit "bitbucket.org/browserlondon/SwanToolkit"
	standard "bitbucket.org/browserlondon/SwanToolkit/SRMStandardSchema"
	log "github.com/sirupsen/logrus"
)

type (
	ProviderSpecificData struct {
		EntityType ProviderSpecificEntityTypes
		Gender     ProviderSpecificGender
	}
	ProviderSpecificEntityTypes struct {
		Individual any
		Entity     any
		Other      any
	}
	ProviderSpecificGender struct {
		Male        string
		Female      string
		Unspecified string
	}

	//Validator
	AdvancedSearchValidator struct {
		EntityType   any           `json:"entity_type,omitempty"  bson:"entity_type"`
		Gender       string        `json:"gender,omitempty"  bson:"gender"`
		DOB          standard.Date `json:"date_of_birth,omitempty"  bson:"date_of_birth"`
		Country      []string      `json:"country,omitempty"  bson:"country"`
		BirthPlace   string        `json:"birth_place,omitempty"  bson:"birth_place"`
		ConvertedDOB string        `json:"converted_date_of_birth,omitempty"  bson:"converted_date_of_birth"`
	}
)

func (v *AdvancedSearchValidator) TransformAndValidateEntityType(providerEntityTypes ProviderSpecificEntityTypes) error {
	switch v.EntityType {
	case standard.STANDARD_SCHEMA_TYPE_INDIVIDUAL:
		v.EntityType = providerEntityTypes.Individual
		return nil
	case standard.STANDARD_SCHEMA_TYPE_ENTITY:
		v.EntityType = providerEntityTypes.Entity
		return nil
	case standard.STANDARD_SCHEMA_TYPE_OTHER:
		v.EntityType = providerEntityTypes.Other
		return nil
	default:
		msg := "invalid entity type"
		err := errors.New(msg)
		log.WithField("Error", err).Error("Entity Transformation and Validation Failed")
		return err
	}
}

func (v *AdvancedSearchValidator) TransformAndValidateGender(providerGender ProviderSpecificGender) error {
	switch v.Gender {
	case standard.MALE_GENDER:
		v.Gender = providerGender.Male
		return nil
	case standard.FEMALE_GENDER:
		v.Gender = providerGender.Female
		return nil
	case standard.UNSPECIFIED_GENDER:
		v.Gender = providerGender.Unspecified
		return nil
	}
	msg := "invalid gender type"
	err := errors.New(msg)
	log.WithField("Error", err).Error("Gender Transformation and Validation Failed")
	return err
}

func (v *AdvancedSearchValidator) TransformAndValidateDoB() error {
	msg := "invalid date of birth"
	layout := "2006-01-02" // Date layout representing "YYYY-MM-DD"
	if v.DOB.FullDate != "" {
		_, err := time.Parse(layout, v.DOB.FullDate)
		if err != nil {
			log.Println("Error parsing date:", err)
			return err
		}
		v.ConvertedDOB = v.DOB.FullDate
		return nil
	}
	if v.DOB.Year > 9999 || v.DOB.Month > 12 || v.DOB.Day > 31 {
		err := errors.New(msg)
		log.WithField("Error", err).Error("Entity Transformation and Validation Failed")
		return err
	}
	month := v.DOB.Month
	day := v.DOB.Day
	if v.DOB.Year != 0 {
		if month == 0 {
			month = 1
		}
		if day == 0 {
			day = 1
		}
		theTime := time.Date(v.DOB.Year, time.Month(month), day, 0, 0, 0, 0, time.Local)
		if v.DOB.Month > 0 {
			if v.DOB.Day > 0 {
				v.ConvertedDOB = theTime.Format(layout)
				return nil
			}
			v.ConvertedDOB = theTime.Format("2006-01")
			return nil
		}
		v.ConvertedDOB = theTime.Format("2006")
		return nil
	}
	err := errors.New(msg)
	log.WithField("Error", err).Error("DoB Transformation and Validation Failed")
	return err
}

func (v *AdvancedSearchValidator) ValidateCountries() error {
	msg := "invalid country code(s)"
	for _, country := range v.Country {
		if len(country) != 3 {
			err := errors.New(msg)
			log.WithField("Error", err).Error("Country Validation Failed")
			return err
		}
	}
	return nil
}

func Validate(filters toolkit.SearchFilter, provider ProviderSpecificData) (*AdvancedSearchValidator, error) {
	validator := AdvancedSearchValidator{
		EntityType: filters.EntityType,
		Gender:     filters.Gender,
		DOB:        filters.DOB,
		Country:    filters.Country,
		BirthPlace: filters.BirthPlace,
	}
	if validator.EntityType != "" {
		err := validator.TransformAndValidateEntityType(provider.EntityType)
		if err != nil {
			return nil, err
		}
	} else {
		validator.EntityType = provider.EntityType.Other
	}

	if validator.Gender != "" {
		err := validator.TransformAndValidateGender(provider.Gender)
		if err != nil {
			return nil, err
		}
	} else {
		validator.Gender = provider.Gender.Unspecified
	}

	if (validator.DOB != standard.Date{}) {
		err := validator.TransformAndValidateDoB()
		if err != nil {
			return nil, err
		}
	}

	if len(validator.Country) != 0 {
		err := validator.ValidateCountries()
		if err != nil {
			return nil, err
		}
	}

	return &validator, nil
}
