package toolkit

import (
	"errors"
	"net/http"
	"os"
	"strings"

	environments "bitbucket.org/browserlondon/SwanToolkit/Environments"
	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
)

func GetConfig(c echo.Context, providerId string, env string) error {

	log.WithFields(log.Fields{
		"providerId": providerId,
		"env":        env,
	}).Info("Config")

	cfg := new(ConfigRequestBody)
	if err := c.Bind(cfg); err != nil {
		log.WithError(err).Error("Failed to bind request")
		return c.NoContent(http.StatusBadRequest)
	}

	if !environments.IsValidEnvironment(cfg.Environment) {
		log.WithError(errors.New("Non-valid Environment")).Error("Environment is not valid")
		return c.NoContent(http.StatusBadRequest)
	}

	if env != cfg.Environment {
		log.WithError(errors.New("Non-equal Environment")).Error("Environment is equal")
		return c.NoContent(http.StatusBadRequest)
	}

	cfgFile := "config/" + providerId + "." + strings.ToLower(cfg.Environment) + ".json"
	_, err := os.Stat(cfgFile)
	if err != nil {
		log.WithError(err).Error("No config file")
		return c.NoContent(http.StatusInternalServerError)
	}

	return c.File(cfgFile)
}
