package environments

import "strings"

const (
	Development = "DEV"
	Staging     = "STAGING"
	Integration = "INTEGRATION"
	Production  = "PRODUCTION"
	Default     = "default"
)

var validEnvironments = []string{Development, Staging, Integration, Production, Default}
var cloudEnvironments = []string{Integration, Staging, Production}

/*
Performs case-ignored comparison to identify if stringSlice contains stringToFind.
*/
func stringSliceContains(stringSlice []string, stringToFind string) bool {
	for _, compString := range stringSlice {
		if strings.ToLower(stringToFind) == strings.ToLower(compString) {
			return true
		}
	}
	return false
}

func IsValidEnvironment(envToCheck string) bool {
	return stringSliceContains(validEnvironments, envToCheck)
}

func IsCloudEnvironment(env string) bool {
	return stringSliceContains(cloudEnvironments, env)
}
