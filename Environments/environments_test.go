package environments_test

import (
	"strings"
	"testing"

	environments "bitbucket.org/browserlondon/SwanToolkit/Environments"

	"github.com/stretchr/testify/assert"
)

func TestIsValidEnvironment(t *testing.T) {
	t.Run("it should return true on exact match", func(t *testing.T) {
		result := environments.IsValidEnvironment(environments.Default)
		assert.Equal(t, true, result)
	})
	t.Run("it should return true on case insensitive match", func(t *testing.T) {
		result := environments.IsValidEnvironment(strings.ToUpper(environments.Default))
		assert.Equal(t, true, result)
	})
	t.Run("it should return false on no match", func(t *testing.T) {
		result := environments.IsValidEnvironment("invalid environment")
		assert.Equal(t, false, result)
	})
}

func TestIsCloudEnvironment(t *testing.T) {
	t.Run("it should return true on exact match", func(t *testing.T) {
		result := environments.IsCloudEnvironment(environments.Integration)
		assert.Equal(t, true, result)
	})
	t.Run("it should return true on case insensitive match", func(t *testing.T) {
		result := environments.IsCloudEnvironment(strings.ToLower(environments.Integration))
		assert.Equal(t, true, result)
	})
	t.Run("it should return false on no match", func(t *testing.T) {
		result := environments.IsCloudEnvironment("invalid environment")
		assert.Equal(t, false, result)
	})
}
