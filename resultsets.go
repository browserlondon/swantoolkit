package toolkit

import (
	"encoding/json"

	srmstandardschema "bitbucket.org/browserlondon/SwanToolkit/SRMStandardSchema"
)

const (
	ActionQuery     = "query"
	ActionFetch     = "fetch"
	ActionRelated   = "related"
	ActionTranslate = "translate"
	ActionRetrieve  = "retrieve"
)

type (
	DataProviderResultMetadata struct {
		ProjectId        string            `json:"project_id"`
		SearchId         string            `json:"search_id"`
		QueryId          string            `json:"query_id"`
		Provider         string            `json:"provider"`
		HitCount         int64             `json:"hit_count"`
		HitCountRelation string            `json:"hit_count_relation"`
		Page             int64             `json:"page"`
		SizeLimit        int               `json:"size_limit"`
		Size             int               `json:"size"`
		Context          string            `json:"context"`
		Facets           []MetaFacetBucket `json:"facets"`
	}

	DataProviderResultSetError struct {
		Code    string `json:"code"`
		Message string `json:"message"`
	}

	DataProviderResultSet struct {
		Action     string                             `json:"action"`
		Metadata   DataProviderResultMetadata         `json:"metadata"`
		Error      DataProviderResultSetError         `json:"error"`
		RawFacets  json.RawMessage                    `json:"raw_facets"`
		Raw        json.RawMessage                    `json:"raw"`
		SRMSchemas []srmstandardschema.StandardSchema `json:"srm_schemas"`
	}

	MetaFacet struct {
		ProviderControl string            `json:"provider_control"`
		Name            string            `json:"name"`
		Type            string            `json:"type"`
		Buckets         []MetaFacetBucket `json:"buckets"`
	}

	MetaFacetBucket struct {
		Value   interface{} `json:"value"`
		Display string      `json:"display"`
		Count   int64       `json:"count"`
	}
)
