package selfregistration

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	log "github.com/sirupsen/logrus"
	"golang.org/x/exp/slices"
)

type (
	RegisterConfigRequest struct {
		Environment string `json:"env"`
		ProviderId  string `json:"providerId"`
		Config      []byte `json:"config"`
	}
)

const (
	RegisterConfigRetryCount = 5
)

func (c *Controller) RegisterConfig(params RegisterConfigRequest, waitDurationSeconds time.Duration) (string, error) {
	log.WithField("ApiUrl", c.ApiUrl).
		WithField("Environment", params.Environment).
		WithField("ProviderId", params.ProviderId).
		Info("Registering service config")

	requestBody, err := json.Marshal(&params)
	if err != nil {
		log.WithField("Error", err).Error("Failed to marsh register config request body")
		return "", err
	}

	retryCodes := GetRegisterConfigRetryCodes()
	url, err := url.JoinPath(c.ApiUrl, "/register")
	if err != nil {
		log.WithField("Error", err).Error("Failed to create register url")
		return "", err
	}

	// Prevent racing, and give time to echo to initialize the /config first before sending the request
	time.Sleep(waitDurationSeconds * time.Second)

	for i := 0; i < RegisterConfigRetryCount; i++ {

		req, err := c.prepareRequest(http.MethodPost, url, bytes.NewBuffer(requestBody))
		if err != nil {
			log.WithField("error", err).Error("Register request preparation failure")
			return "", err
		}

		instanceId, status, err := c.sendRequest(req)
		if err != nil {
			if slices.Contains(retryCodes, status) {
				// Retryable status code. Wait, then go to next loop.
				time.Sleep(time.Second)
			} else {
				log.WithField("error", err).Error("Register send request failure")
				return "", err
			}
		} else {
			log.WithField("Instance ID", instanceId).Info("Registered provider config with API service")
			return instanceId, nil
		}
	}

	return "", fmt.Errorf("reached maximum retry count: %d", RegisterConfigRetryCount)
}

func GetRegisterConfigRetryCodes() []int {
	return []int{408, 425, 429, 500, 503, 503, 504}
}
