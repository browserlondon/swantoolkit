package selfregistration

import (
	"net/http"
	"net/url"

	log "github.com/sirupsen/logrus"
)

func (c *Controller) HealthCheck() (string, error) {
	url, err := url.JoinPath(c.ApiUrl, "/health/id")
	if err != nil {
		log.WithField("Error", err).Fatal("Could not build healthcheck path")
		return "", err
	}

	req, err := c.prepareRequest(http.MethodGet, url, nil)
	if err != nil {
		log.WithField("error", err).Error("Healthcheck request preparation failure")
		return "", err
	}

	instanceId, _, err := c.sendRequest(req)
	if err != nil {
		log.WithField("error", err).Error("Healthcheck send request failure")
		return "", err
	}

	return instanceId, nil
}
