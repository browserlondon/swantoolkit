package selfregistration_test

import (
	"net/http"
	"testing"

	selfregistration "bitbucket.org/browserlondon/SwanToolkit/SelfRegistration"
	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
)

func TestRegisterConfig(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(
		"POST",
		"http://test.success/register",
		httpmock.NewStringResponder(200, `{"instanceId": "MOCKINSTANCEUUID"}`),
	)

	httpmock.RegisterResponder(
		"POST",
		"http://test.fail/register",
		httpmock.NewStringResponder(500, ""),
	)

	retryCount := 1
	httpmock.RegisterResponder(
		"POST",
		"http://test.retry/register",
		func(req *http.Request) (*http.Response, error) {
			if retryCount < 5 {
				retryCount += 1
				return httpmock.NewStringResponse(500, ""), nil
			}

			return httpmock.NewStringResponse(200, `{"instanceId": "MOCKINSTANCEUUID"}`), nil
		},
	)

	httpmock.RegisterResponder(
		"POST",
		"http://test.apikey/register",
		func(req *http.Request) (*http.Response, error) {
			if req.Header.Get("api-key") != "MOCKKEY" {
				return httpmock.NewStringResponse(500, ""), nil
			}

			return httpmock.NewStringResponse(200, `{"instanceId": "MOCKINSTANCEUUID"}`), nil
		},
	)

	t.Run("request has api-key header", testApiKeyHeader)
	t.Run("request completes", testRegisterSuccess)
	t.Run("request fails", testRegisterFail)
	t.Run("request is retried at least 5 times on failure", testRetryCount)
	t.Run("request is retrying the correct status codes", testValidRetryCodes)
}

func testApiKeyHeader(t *testing.T) {
	controller := selfregistration.Controller{
		ApiUrl: "http://test.apikey",
		ApiKey: "MOCKKEY",
	}
	params := selfregistration.RegisterConfigRequest{
		Environment: "dev",
		ProviderId:  "testprovider",
		Config:      []byte("json_string"),
	}

	_, err := controller.RegisterConfig(params, 2)
	assert.Equal(t, nil, err)
}

func testRegisterSuccess(t *testing.T) {
	controller := selfregistration.Controller{
		ApiUrl: "http://test.success",
		ApiKey: "MOCKKEY",
	}
	params := selfregistration.RegisterConfigRequest{
		Environment: "dev",
		ProviderId:  "testprovider",
		Config:      []byte("json_string"),
	}

	instanceId, err := controller.RegisterConfig(params, 2)
	assert.Equal(t, "MOCKINSTANCEUUID", instanceId)
	assert.Equal(t, nil, err)
}

func testRegisterFail(t *testing.T) {
	controller := selfregistration.Controller{
		ApiUrl: "http://test.fail",
		ApiKey: "MOCKKEY",
	}
	params := selfregistration.RegisterConfigRequest{
		Environment: "dev",
		ProviderId:  "testprovider",
		Config:      []byte("json_string"),
	}

	_, err := controller.RegisterConfig(params, 2)
	assert.NotEqual(t, nil, err)
}

func testRetryCount(t *testing.T) {
	controller := selfregistration.Controller{
		ApiUrl: "http://test.retry",
		ApiKey: "MOCKKEY",
	}
	params := selfregistration.RegisterConfigRequest{
		Environment: "dev",
		ProviderId:  "testprovider",
		Config:      []byte("json_string"),
	}

	_, err := controller.RegisterConfig(params, 2)
	assert.Equal(t, nil, err)
}

func testValidRetryCodes(t *testing.T) {
	assert.Equal(t, []int{408, 425, 429, 500, 503, 503, 504}, selfregistration.GetRegisterConfigRetryCodes())
}
