package selfregistration

import (
	"errors"
	"io/ioutil"
	"strings"

	environments "bitbucket.org/browserlondon/SwanToolkit/Environments"
	log "github.com/sirupsen/logrus"
)

func getConfigData(providerId string, env string) ([]byte, error) {
	log.WithFields(log.Fields{
		"providerId": providerId,
		"env":        env,
	}).Info("Config")
	var err error

	if !environments.IsValidEnvironment(env) {
		err = errors.New("Non-valid Environment")
		log.WithError(err).Error("Environment is not valid")
		return nil, err
	}

	cfgFile := "config/" + providerId + "." + strings.ToLower(env) + ".json"
	config, err := ioutil.ReadFile(cfgFile)
	if err != nil {
		log.WithError(err).Error("No config file")
		return nil, err
	}

	return config, nil
}
