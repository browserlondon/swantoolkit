package selfregistration

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

type (
	Controller struct {
		ApiUrl string
		ApiKey string
	}

	InstanceIdResponse struct {
		InstanceId string `json:"instanceId"`
	}
)

var (
	controller Controller
)

const (
	EMITTER_CONFIG_STATUS = "provider.config_updated"
)

func Run(apiUrl string, apiKey string, providerId string, env string) error {
	controller = Controller{
		ApiUrl: apiUrl,
		ApiKey: apiKey,
	}
	config, err := getConfigData(providerId, env)
	if err != nil {
		return err
	}
	params := RegisterConfigRequest{
		Environment: env,
		ProviderId:  providerId,
		Config:      config,
	}
	instanceId, err := controller.RegisterConfig(params, 2)
	if err != nil {
		return err
	}

	interval := time.Minute
	tick := time.NewTicker(interval)
	for range tick.C {
		checkInstanceId, err := controller.HealthCheck()
		if err != nil {
			log.WithField("Error", err).Error("Failed to connect, API service is down!")
		} else if checkInstanceId != instanceId {
			log.WithField("Error", errors.New("Instance ID does not match!")).Error("Instance ID is different, reregistering with API service!", instanceId, checkInstanceId)
			instanceId, err = controller.RegisterConfig(params, 0)
			if err != nil {
				return err
			}
		} else {
			log.Info("Healthcheck successful!")
		}
	}
	return nil
}

func (c *Controller) prepareRequest(requestMethod string, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(requestMethod, url, body)
	if err != nil {
		return &http.Request{}, fmt.Errorf("Failed to create request: %v", err)
	}

	req.Header.Set("api-key", c.ApiKey)
	req.Header.Set("Content-Type", "application/json")

	return req, nil
}

func (c *Controller) sendRequest(req *http.Request) (string, int, error) {
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", http.StatusInternalServerError, fmt.Errorf("Failed to perform request: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return "", resp.StatusCode, fmt.Errorf("received invalid response code: %d", resp.StatusCode)
	}

	responseBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", http.StatusInternalServerError, fmt.Errorf("Failed to parse response: %v", err)
	}

	var response InstanceIdResponse
	if err := json.Unmarshal(responseBody, &response); err != nil {
		return "", http.StatusInternalServerError, fmt.Errorf("Failed to unmarshal response: %v", err)
	}

	return response.InstanceId, resp.StatusCode, nil

}
