package selfregistration_test

import (
	"net/http"
	"testing"

	selfregistration "bitbucket.org/browserlondon/SwanToolkit/SelfRegistration"

	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
)

func TestHealthCeck(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(
		"GET",
		"http://test.apikey/health/id",
		func(req *http.Request) (*http.Response, error) {
			if req.Header.Get("api-key") != "MOCKKEY" {
				return httpmock.NewStringResponse(500, ""), nil
			}

			return httpmock.NewStringResponse(200, `{"instanceId": "MOCKINSTANCEUUID"}`), nil
		},
	)

	httpmock.RegisterResponder(
		"GET",
		"http://test.success/health/id",
		httpmock.NewStringResponder(200, `{"instanceId": "MOCKINSTANCEUUID"}`),
	)

	httpmock.RegisterResponder(
		"GET",
		"http://test.fail/health/id",
		httpmock.NewStringResponder(404, ""),
	)

	t.Run("request has api-key header", testHealthCheckApiKeyHeader)
	t.Run("request completes", testHealthCheckSuccess)
	t.Run("request fails", testHealthCheckFail)
}

func testHealthCheckApiKeyHeader(t *testing.T) {
	controller := selfregistration.Controller{
		ApiUrl: "http://test.apikey",
		ApiKey: "MOCKKEY",
	}
	_, err := controller.HealthCheck()
	assert.Equal(t, nil, err)
}

func testHealthCheckSuccess(t *testing.T) {
	controller := selfregistration.Controller{
		ApiUrl: "http://test.success",
		ApiKey: "MOCKKEY",
	}

	instanceId, err := controller.HealthCheck()
	assert.Equal(t, "MOCKINSTANCEUUID", instanceId)
	assert.Equal(t, nil, err)
}

func testHealthCheckFail(t *testing.T) {
	controller := selfregistration.Controller{
		ApiUrl: "http://test.fail",
		ApiKey: "MOCKKEY",
	}

	_, err := controller.HealthCheck()
	assert.NotEqual(t, nil, err)
}
