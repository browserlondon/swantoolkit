package toolkit

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/vmihailenco/msgpack/v5"
)

func SendToProcessing(processingUrl string, doc DataProviderResultSet, format string, apiKey string) (err error) {
	var payload []byte
	if format == "msgpack" {
		payload, err = msgpack.Marshal(&doc)
	} else {
		payload, err = json.Marshal(&doc)
	}
	if err != nil {
		log.WithError(err).Error("Could not encode result set for transfer")
		return
	}

	req, err := http.NewRequest("POST", processingUrl, bytes.NewBuffer(payload))
	if format == "msgpack" {
		req.Header.Set("Content-Type", "application/x-msgpack")
	} else {
		req.Header.Set("Content-Type", "application/json")
	}

	if apiKey != "" {
		req.Header.Set("api-key", apiKey)
	} else {
		err = errors.New("api-key is empty")
		log.WithError(err).Error("Could not post result to processing service (0)")
		return
	}

	if err != nil {
		log.WithError(err).Error("Could not post result to processing service (1)")
		return
	}
	client := &http.Client{}
	resp, err := WaitForServer(1*time.Minute, func() (*http.Response, error) {
		return client.Do(req)
	})
	if err != nil {
		log.WithError(err).Error("Could not post result to processing service (2)")
		return
	}
	if resp.StatusCode != 200 {
		log.WithField("Status code", resp.StatusCode).Error("Non 200 response from processing service")
		return
	}
	_ = resp.Body.Close()

	return
}
