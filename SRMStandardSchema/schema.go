package srmstandardschema

type StandardSchema struct {
	Type       string           `json:"type"` // Can be either Individual or Entity or Other
	Name       string           `json:"name"`
	Sound      string           `json:"sound"` // first name, last name, year of birth for personal, name and country for business
	Countries  []Country        `json:"countries"`
	Addresses  []string         `json:"addresses,omitempty"`
	Individual IndividualSchema `json:"individual_attributes,omitempty"`
	Entity     EntitySchema     `json:"entity_attributes,omitempty"`
	Provider   Provider         `json:"provider"`
}
type EntitySchema struct {
	EntityType string `json:"company_type,omitempty"`
}

type IndividualSchema struct {
	Name         Name   `json:"individual_name"`
	Gender       string `json:"gender,omitempty"`
	DatesOfBirth []Date `json:"dates_of_birth,omitempty"`
}

type Name struct {
	FirstName  string `json:"first_name"`
	MiddleName string `json:"middle_name,omitempty"`
	LastName   string `json:"last_name"`
	FullName   string `json:"full_name"`
}

type Country struct {
	Code string `json:"country_code"`
	Name string `json:"country_name"`
}

type Date struct {
	Day      int    `json:"day,omitempty"`
	Month    int    `json:"month,omitempty"`
	Year     int    `json:"year,omitempty"`
	FullDate string `json:"full_date,omitempty"`
}

type Provider struct {
	Type string `json:"provider_type"`
	Name string `json:"provider_name"`
}

const (
	// Standard Schema Types
	STANDARD_SCHEMA_TYPE_INDIVIDUAL = "Individual"
	STANDARD_SCHEMA_TYPE_ENTITY     = "Entity"
	STANDARD_SCHEMA_TYPE_OTHER      = "Other"

	// Provider IDs
	WORLDCHECK_PROVIDER_ID      = "compliance.worldcheck"
	WORLDCOMPLIANCE_PROVIDER_ID = "compliance.worldcompliance"
	DOWJONES_PROVIDER_ID        = "compliance.dowjones"
	SAYARI_PROVIDER_ID          = "corporate_records.sayari"
	ARACHNYS_PROVIDER_ID        = "news.arachnys"
	FACTIVA_PROVIDER_ID         = "news.factiva"

	// Gender
	MALE_GENDER        = "Male"
	FEMALE_GENDER      = "Female"
	UNSPECIFIED_GENDER = "Unspecified"
)

var STANDARD_SCHEMA_TYPES = []string{STANDARD_SCHEMA_TYPE_INDIVIDUAL, STANDARD_SCHEMA_TYPE_ENTITY, STANDARD_SCHEMA_TYPE_OTHER}

var PROVIDER_LIST = []string{WORLDCHECK_PROVIDER_ID, WORLDCOMPLIANCE_PROVIDER_ID, DOWJONES_PROVIDER_ID, SAYARI_PROVIDER_ID, ARACHNYS_PROVIDER_ID, FACTIVA_PROVIDER_ID}

var COMPLIANCE_PROVIDER_LIST = []string{WORLDCHECK_PROVIDER_ID, WORLDCOMPLIANCE_PROVIDER_ID, DOWJONES_PROVIDER_ID}
