package toolkit

import (
	"net"
	"net/url"
	"strconv"
	"strings"
)

func DiscoverService(host string) string {
	parsedUrl, err := url.Parse(host)
	if err == nil {
		if strings.Contains(parsedUrl.Host, ".swan") {
			_, addrs, err := net.LookupSRV("", "", parsedUrl.Host)
			if err == nil {
				for _, addr := range addrs {
					parsedUrl.Host = addr.Target + ":" + strconv.Itoa(int(addr.Port))
					return parsedUrl.String()
				}
			}
		}
	}

	return host
}
