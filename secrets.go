package toolkit

import (
	"encoding/json"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/secretsmanager"
	log "github.com/sirupsen/logrus"
)

func GetSecrets(secretName string) (config map[string]string) {
	region := "eu-west-1"

	//Create a Secrets Manager client
	svc := secretsmanager.New(session.New(),
		aws.NewConfig().WithRegion(region))
	input := &secretsmanager.GetSecretValueInput{
		SecretId:     aws.String(secretName),
		VersionStage: aws.String("AWSCURRENT"), // VersionStage defaults to AWSCURRENT if unspecified
	}

	// In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
	// See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html

	result, err := svc.GetSecretValue(input)
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case secretsmanager.ErrCodeDecryptionFailure:
				// Secrets Manager can't decrypt the protected secret text using the provided KMS key.
				log.WithError(aerr).Fatal(secretsmanager.ErrCodeDecryptionFailure)

			case secretsmanager.ErrCodeInternalServiceError:
				// An error occurred on the server side.
				log.WithError(aerr).Fatal(secretsmanager.ErrCodeInternalServiceError)

			case secretsmanager.ErrCodeInvalidParameterException:
				// You provided an invalid value for a parameter.
				log.WithError(aerr).Fatal(secretsmanager.ErrCodeInvalidParameterException)

			case secretsmanager.ErrCodeInvalidRequestException:
				// You provided a parameter value that is not valid for the current state of the resource.
				log.WithError(aerr).Fatal(secretsmanager.ErrCodeInvalidRequestException)

			case secretsmanager.ErrCodeResourceNotFoundException:
				// We can't find the resource that you asked for.
				log.WithError(aerr).Fatal(secretsmanager.ErrCodeResourceNotFoundException)
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			log.WithError(err).Fatal("Error fetching credentials from AWS")

		}
		return
	}

	// Decrypts secret using the associated KMS CMK.
	// Depending on whether the secret is a string or binary, one of these fields will be populated.
	//var secretString, decodedBinarySecret string
	//if result.SecretString != nil {
	//secretString := *result.SecretString
	//} else {
	//	decodedBinarySecretBytes := make([]byte, base64.StdEncoding.DecodedLen(len(result.SecretBinary)))
	//	len, err := base64.StdEncoding.Decode(decodedBinarySecretBytes, result.SecretBinary)
	//	if err != nil {
	//		log.WithError(err).Fatal("Base64 Decode Error")
	//		return
	//	}
	//	decodedBinarySecret = string(decodedBinarySecretBytes[:len])
	//}

	json.Unmarshal([]byte(*result.SecretString), &config)

	return config
}
