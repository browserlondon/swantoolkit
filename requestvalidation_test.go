package toolkit

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidateRequest(t *testing.T) {
	t.Run("it should throw error no api-key header is given", func(t *testing.T) {

		req := httptest.NewRequest(http.MethodGet, "http://localhost", nil)
		expectedError := errors.New("empty api-key header")

		actualIsValid, actualErr := ValidateRequest(*req, "I_AM_API_KEY")

		assert.Equal(t, false, actualIsValid)
		assert.Equal(t, expectedError, actualErr)

	})

	t.Run("it should throw error when api-key doesn't match", func(t *testing.T) {

		req := httptest.NewRequest(http.MethodGet, "http://localhost", nil)
		req.Header.Add("api-key", "WRONG_API_KEY")
		expectedError := errors.New("api-key not matching")

		actualIsValid, actualErr := ValidateRequest(*req, "I_AM_API_KEY")

		assert.Equal(t, false, actualIsValid)
		assert.Equal(t, expectedError, actualErr)

	})

	t.Run("it should pass with matching api key", func(t *testing.T) {

		req := httptest.NewRequest(http.MethodGet, "http://localhost", nil)
		req.Header.Add("api-key", "I_AM_API_KEY")

		actualIsValid, actualErr := ValidateRequest(*req, "I_AM_API_KEY")

		assert.Equal(t, true, actualIsValid)
		assert.Equal(t, nil, actualErr)
	})

}
