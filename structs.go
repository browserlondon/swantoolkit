package toolkit

import (
	srmstandardschema "bitbucket.org/browserlondon/SwanToolkit/SRMStandardSchema"
	"github.com/labstack/echo/v4"
)

type (
	QueryStructureComponent struct {
		Type     string                    `json:"type" validate:"required"`
		Value    string                    `json:"value" validate:"required"`
		Children []QueryStructureComponent `json:"children,omitempty"`
	}

	QueryRequest struct {
		ProjectId   string                    `json:"project" validate:"required"`
		SearchId    string                    `json:"search" validate:"required"`
		QueryId     string                    `json:"query" validate:"required"`
		QueryStruct []QueryStructureComponent `json:"query_struct"  validate:"required,gte=1"`
		QueryFacets map[string][]string       `json:"query_facets"`
		Sort        string                    `json:"sort"`
		Page        int64                     `json:"page"`
		Offset      int64                     `json:"offset"`
		Limit       int64                     `json:"limit"`
		Context     string                    `json:"context"`
		Filter      SearchFilter              `json:"filter,omitempty"` //advanced search fields
	}

	FetchRequest struct {
		ProjectId string   `json:"project_id" validate:"required" swaggerignore:"true"`
		Ids       []string `json:"ids" validate:"required,gte=1"`
	}

	RelatedRequest struct {
		ProjectId string   `json:"project_id" validate:"required" swaggerignore:"true"`
		SearchId  string   `json:"search_id" validate:"required"`
		URLs      []string `json:"urls" validate:"required,gte=1"`
	}

	SimpleErrorList struct {
		Errors []string `json:"errors"`
	}

	/**
	* General Info:
	* Country: The country ISO-3166-2 Alpha-3 Code - for more information please visit https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes
	* DOB: Standard format for DOB is YYYY-MM-DD
	* IsExcludeQuery: To perform exclude query
	**/

	SearchFilter struct {
		EntityType     string                 `json:"entity_type,omitempty"`                              // Individual, Entity or Other (case sensitive)
		Gender         string                 `json:"gender,omitempty"`                                   // Male, Female, Unspecified
		DOB            srmstandardschema.Date `json:"date_of_birth,omitempty"`                            // Day, Month, Year then we construct the FullDate as YYYY-MM-DD
		Country        []string               `json:"country,omitempty"`                                  // ISO-3166-2 Alpha-3 Code
		BirthPlace     string                 `json:"birth_place,omitempty" bson:"birth_place"`           // ISO-3166-2 Alpha-3 Code
		IsExcludeQuery bool                   `json:"is_exclude_query,omitempty" bson:"is_exclude_query"` //TODO for negative filtering
	}
)

type ConfigRequestBody struct {
	Environment string `json:"env"`
}

func (l *SimpleErrorList) Add(message string) {
	l.Errors = append(l.Errors, message)
}

func (l SimpleErrorList) Count() int {
	return len(l.Errors)
}

func (l SimpleErrorList) HasErrors() bool {
	return len(l.Errors) > 0
}

func ExtractQueryRequest(c echo.Context) (r QueryRequest, errList SimpleErrorList) {
	if err := c.Bind(&r); err != nil {
		errList.Add("Cannot parse request body - is it valid JSON?")
		return r, errList
	}

	// Validate the request
	if errList := Validate(r); errList.HasErrors() {
		return r, errList
	}

	for _, queryStruct := range r.QueryStruct {
		if errList := ValidatedNested(queryStruct, "query_struct"); errList.HasErrors() {
			return r, errList
		}
	}

	return r, errList
}

func ExtractFetchRequest(c echo.Context) (r FetchRequest, errList SimpleErrorList) {
	if err := c.Bind(&r); err != nil {
		errList.Add("Cannot parse request body - is it valid JSON?")
		return r, errList
	}

	// Validate the request
	if errList := Validate(r); errList.HasErrors() {
		return r, errList
	}

	return r, errList
}

func ExtractRelatedRequest(c echo.Context) (r RelatedRequest, errList SimpleErrorList) {
	if err := c.Bind(&r); err != nil {
		errList.Add("Cannot parse request body - is it valid JSON?")
		return r, errList
	}

	// Validate the request
	if errList := Validate(r); errList.HasErrors() {
		return r, errList
	}

	return r, errList
}
