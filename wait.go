package toolkit

import (
	"fmt"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
)

func Retry(maxRetries int, sleep time.Duration, fn func() (*http.Response, error)) (*http.Response, error) {
	var lastError error
	for i := 0; i < maxRetries; i++ {
		res, err := fn()
		if err == nil {
			return res, nil
		}
		lastError = err
		time.Sleep(sleep)
	}
	return nil, fmt.Errorf("maximum retries reached: %v", lastError)
}

// WaitForServer attempts to run the API Call function
// It tries for one minute using exponential back-off.
// It reports an error if all attempts fail.
func WaitForServer(timeout time.Duration, fn func() (*http.Response, error)) (*http.Response, error) {
	// timeout = 1 * time.Minute
	log.Println("Waiting server started")
	deadline := time.Now().Add(timeout)
	for tries := 0; time.Now().Before(deadline); tries++ {
		res, err := fn()
		if err == nil {
			return res, nil // success
		}
		log.Printf("server not responding (%s); retrying...", err)
		time.Sleep(time.Second << uint(tries)) // exponential back-off
	}
	return nil, fmt.Errorf("server failed to respond after %s", timeout)
}
