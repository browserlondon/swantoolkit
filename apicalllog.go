package toolkit

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/vmihailenco/msgpack/v5"
)

type (
	ApiCallLogEntry struct {
		When             time.Time           `json:"when"`
		Environment      string              `json:"environment"`   // staging
		QueryId          string              `json:"query_id"`      // UUID
		Provider         string              `json:"provider"`      // news.factiva
		FunctionName     string              `json:"function_name"` // PerformContentSearchWithFDTDedupEx
		Input            string              `json:"input"`         // Querystring or context etc
		InputFacets      map[string][]string `json:"input_facets"`
		RequestUrl       string              `json:"request_url"`
		RequestBody      string              `json:"request_body"`
		Success          bool                `json:"success"`
		ErrorMessage     string              `json:"error_message"`
		HitCount         int64               `json:"hit_count"`
		HitCountRelation string              `json:"hit_count_relation"`
		Page             int64               `json:"page"`
		PageSize         int64               `json:"page_size"`
		TimeTaken        int64               `json:"time_taken"`
	}
)

func LogApiCall(url string, entry ApiCallLogEntry, format string, apiKey string) (err error) {
	var payload []byte
	if format == "msgpack" {
		payload, err = msgpack.Marshal(&entry)
	} else {
		payload, err = json.Marshal(&entry)
	}
	if err != nil {
		log.WithError(err).Error("Could not encode api call log entry for transfer")
		return
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(payload))
	if format == "msgpack" {
		req.Header.Set("Content-Type", "application/x-msgpack")
	} else {
		req.Header.Set("Content-Type", "application/json")
	}

	if apiKey != "" {
		req.Header.Set("api-key", apiKey)
	} else {
		err = errors.New("api-key is empty")
		log.WithError(err).Error("Could not post result to processing service (0)")
		return
	}

	if err != nil {
		log.WithError(err).Error("Could not post api call log entry to processing service (1)")
		return
	}

	client := &http.Client{}
	resp, err := WaitForServer(1*time.Minute, func() (*http.Response, error) {
		return client.Do(req)
	})
	if err != nil {
		log.WithError(err).Error("Could not post api call log entry to processing service (2)")
		return
	}
	if resp.StatusCode != 200 {
		log.WithField("Status code", resp.StatusCode).Error("Non 200 response from processing service (api call log)")
		return
	}
	_ = resp.Body.Close()

	return nil
}
