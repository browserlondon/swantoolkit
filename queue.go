package toolkit

import (
	"container/heap"
	"context"
	"errors"
	log "github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
	"sync"
	"time"
)

// Priority queue library: https://github.com/beeker1121/goque
// Seems to have a mutex, but can't see how exactly it works
// This seems to take the same approach, but actually uses the lock: https://stackoverflow.com/questions/28809094/simple-concurrent-queue

// Priority queue as a heap: https://godoc.org/container/heap#example-package--PriorityQueue
// Keeping it safe: https://husobee.github.io/heaps/golang/safe/2016/09/01/safe-heaps-golang.html

// https://golang.org/pkg/container/heap/ - Has PriorityQueue example

const (
	QueuePriorityHigh   = 80
	QueuePriorityMedium = 50
	QueuePriorityLow    = 20
)

type (
	PriorityQueueItem struct {
		Action   string
		Priority int
		Item     interface{}
		index    int
	}

	PriorityQueueData []*PriorityQueueItem

	// A PriorityQueue implements heap.Interface
	PriorityQueue struct {
		d     *PriorityQueueData
		m     sync.Mutex
		count uint64
	}
)

// Sets up a new priority queue
func NewPriorityQueue() PriorityQueue {
	d := make(PriorityQueueData, 0)
	heap.Init(&d)
	return PriorityQueue{
		d: &d,
		m: sync.Mutex{},
	}
}

func (q *PriorityQueue) Push(i *PriorityQueueItem) {
	q.m.Lock()
	defer q.m.Unlock()
	heap.Push(q.d, i)
	log.Info("Added to queue")
}

func (q *PriorityQueue) Pull() (*PriorityQueueItem, error) {
	if q.Len() == 0 {
		return &PriorityQueueItem{}, errors.New("empty")
	}
	q.m.Lock()
	defer q.m.Unlock()
	return heap.Pop(q.d).(*PriorityQueueItem), nil
}

func (q PriorityQueue) Len() int {
	return q.d.Len()
}

// Polls the queue at a set rate and inserts into a channel for workers to pull from at their own speed
func (q PriorityQueue) DispatchToWorkerChannel(jobs chan PriorityQueueItem, rateLimit time.Duration, maxJobsChannelSize int) {
	ctx := context.Background()
	limiter := rate.NewLimiter(rate.Every(rateLimit), 1)

	for {
		err := limiter.Wait(ctx)
		if err != nil {
			log.WithError(err).Info("Rate limiting error")
		}

		// Ensure the jobs channel isn't full
		// This means that if all the workers are still busy then we keep items in our queue
		if len(jobs) > maxJobsChannelSize {
			continue
		}

		item, err := q.Pull()
		if err != nil {
			continue
		} else {
			jobs <- *item
		}
	}
}

// Heap functions

func (d PriorityQueueData) Len() int {
	return len(d)
}

func (d PriorityQueueData) Less(i, j int) bool {
	// We want Pop to give us the highest, not lowest, priority so we use greater than here.
	return d[i].Priority > d[j].Priority
}

func (d PriorityQueueData) Swap(i, j int) {
	d[i], d[j] = d[j], d[i]
	d[i].index = i
	d[j].index = j
}

func (d *PriorityQueueData) Push(x interface{}) {
	n := len(*d)
	item := x.(*PriorityQueueItem)
	item.index = n
	*d = append(*d, item)
}

func (d *PriorityQueueData) Pop() interface{} {
	old := *d
	n := len(old)
	item := old[n-1]
	old[n-1] = nil  // avoid memory leak
	item.index = -1 // for safety
	*d = old[0 : n-1]
	return item
}
