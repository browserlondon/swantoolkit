package toolkit

import (
	"net/http"
	"reflect"
	"strings"

	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	entranslations "github.com/go-playground/validator/v10/translations/en"
	"github.com/labstack/echo/v4"
	log "github.com/sirupsen/logrus"
)

func Validate(input interface{}) (errList SimpleErrorList) {
	return ValidatedNested(input, "")
}

func ValidatedNested(input interface{}, prefix string) (errList SimpleErrorList) {
	english := en.New()
	uni := ut.New(english, english)
	trans, _ := uni.GetTranslator("en")

	validate := validator.New()
	validate.RegisterTagNameFunc(func(fld reflect.StructField) string {
		name := strings.SplitN(fld.Tag.Get("json"), ",", 2)[0]

		if name == "-" {
			return ""
		}

		return name
	})

	entranslations.RegisterDefaultTranslations(validate, trans)

	validationErrors := validate.Struct(input)

	if validationErrors != nil {
		for _, err := range validationErrors.(validator.ValidationErrors) {
			if prefix != "" {
				errList.Add(prefix + "." + err.Translate(trans))
				log.Error(prefix + "." + err.Translate(trans))
			} else {
				errList.Add(err.Translate(trans))
				log.Error(err.Translate(trans))
			}
		}
	}

	return errList
}

func ValidationErrorListResponse(c echo.Context, errList SimpleErrorList) error {
	return c.JSON(http.StatusBadRequest, errList.Errors)
}
