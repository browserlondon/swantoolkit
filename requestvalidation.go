package toolkit

import (
	"errors"
	"net/http"
)

func ValidateRequest(request http.Request, apiKey string) (bool, error) {
	apiKeyHeaderValue := request.Header.Get("api-key")

	if apiKeyHeaderValue == "" {
		return false, errors.New("empty api-key header")
	}

	if apiKeyHeaderValue != apiKey {
		return false, errors.New("api-key not matching")
	}

	return true, nil
}
